# Changelog

## 0.2.1

- Updated dependencies
- Added function to get manifest dir of final target

## 0.2.0

- Proper repo organisation
