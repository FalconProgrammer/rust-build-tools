extern crate zip;

use std::env;
use std::path::{Path, PathBuf};

pub fn unzip_win_build_files<I: IntoIterator<Item = String>>(zip_file_name: &Path, files: I) -> Result<(), String> {
	// NOTE (JB) Open the Zip File
	let file = std::fs::File::open(zip_file_name.to_str().unwrap()).unwrap();
	let mut archive = zip::ZipArchive::new(file).unwrap();

	// NOTE (JB) Determine the target directory (to place dlls)
	let bin_dir = find_cargo_target_dir();
	let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

	for a in files.into_iter() {
		// NOTE (JB) Get the file from the archive
		let mut file = match archive.by_name(&a) {
			Ok(f) => f,
			_ => return Err(String::from(format!("Unable to find [ {} ]", a))),
		};

		// NOTE (JB) Determine output dir/filename
		let outfile_path = Path::new(&a).file_name().unwrap();

		let outfile_full_path = if a.contains("dll") {
			bin_dir.join(outfile_path)
		} else {
			PathBuf::from(out_dir.join(outfile_path))
		};

		// NOTE (JB) Output the file
		let mut outfile = std::fs::OpenOptions::new()
			.write(true)
			.truncate(true)
			.create(true)
			.open(outfile_full_path)
			.unwrap();

		// NOTE (JB) Write the file
		std::io::copy(&mut file, &mut outfile).unwrap();
	}

	Ok(())
}

use bzip2::read::BzDecoder;
use flate2::read::GzDecoder;
use tar::Archive;
use std::io::{Read, Write};

pub fn untar_win_build_files<I: IntoIterator<Item = String>>(tar_file_name: &Path, files: I) -> Result<(), String> {
	enum TarCompress {
		GZ,
		BZ2,
		TAR,
	};

	let comp_type = match tar_file_name.extension().unwrap().to_str().unwrap() {
		"gz" => TarCompress::GZ,
		"bz2" => TarCompress::BZ2,
		"tar" => TarCompress::TAR,
		_ => return Err(String::from(format!("Unknown file extension [ {} ]", tar_file_name.to_str().unwrap()))),
	};

	let file = std::fs::File::open(tar_file_name.to_str().unwrap()).unwrap();

	return match comp_type {
		TarCompress::GZ => {
			let gz_comp = GzDecoder::new(file);

			untar(files, &mut Archive::new(gz_comp))
		}
		TarCompress::BZ2 => {
			let bz2_comp = BzDecoder::new(file);

			untar(files, &mut Archive::new(bz2_comp))
		}
		TarCompress::TAR => untar(files, &mut Archive::new(file)),
	};
}

fn untar<I: IntoIterator<Item = String>, A: std::io::Read>(files: I, archive: &mut Archive<A>) -> Result<(), String> {
	// NOTE (JB) Determine the target directory (to place dlls)
	let bin_dir = find_cargo_target_dir();
	let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

	// NOTE (JB) Collect all files to extract, and entries
	let all_files: Vec<String> = files.into_iter().collect();
	let entries = archive.entries().unwrap();

	// NOTE (JB) Output all the files in their assigned directories
	for a in entries {

		let mut entry = a.unwrap();

		// NOTE (JB) Get the Entry Paths
		let entry_path = entry.path().unwrap();
		let entry_path_filename = entry_path.file_name().unwrap().to_str().unwrap().to_string();

		if all_files.contains(&entry_path.to_str().unwrap().to_string()) {
			let outfile_full_path = if entry_path_filename.contains("dll") {
				bin_dir.join(entry_path_filename)
			} else {
				PathBuf::from(out_dir.join(entry_path_filename))
			};

			dbg!(&outfile_full_path);

			let res = entry.unpack(outfile_full_path);

			match res {
				Ok(ref r) => {
					dbg!(r);
				}
				Err(ref e) => {
					dbg!(e);
				}
			}
		}
	}

	Ok(())
}

fn run_command(cmd: &str, args: &[&str]) {
	use std::process::Command;
	match Command::new(cmd).args(args).output() {
		Ok(output) => {
			if !output.status.success() {
				let error = std::str::from_utf8(&output.stderr).unwrap();
				panic!("Command '{}' failed: {}", cmd, error);
			}
		}
		Err(error) => {
			panic!("Error running command '{}': {:#}", cmd, error);
		}
	}
}

pub fn download_to(url: &str, dest: &str) {
	if cfg!(windows) {
		run_command(
			"powershell",
			&[
				"-NoProfile",
				"-NonInteractive",
				"-Command",
				"& {
                $client = New-Object System.Net.WebClient
                $client.DownloadFile($args[0], $args[1])
                if (!$?) { Exit 1 }
            }",
				url,
				dest,
			],
		);
	} else {
		run_command("curl", &[url, "-o", dest]);
	}
}

pub fn find_cargo_target_dir() -> PathBuf {
	// Infer the top level cargo target dir from the OUT_DIR by searching
	// upwards until we get to $CARGO_TARGET_DIR/build/ (which is always one
	// level up from the deepest directory containing our package name)
	let pkg_name = env::var("CARGO_PKG_NAME").unwrap();
	let mut out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
	loop {
		{
			let final_path_segment = out_dir.file_name().unwrap();
			if final_path_segment.to_string_lossy().contains(&pkg_name) {
				break;
			}
		}
		if !out_dir.pop() {
			panic!("Malformed build path: {}", out_dir.to_string_lossy());
		}
	}
	out_dir.pop();
	out_dir.pop();
	out_dir
}

pub fn find_cargo_target_manifest_dir() -> PathBuf {
	let mut target_dir = find_cargo_target_dir();

	target_dir.pop();
	target_dir.pop();

	target_dir
}